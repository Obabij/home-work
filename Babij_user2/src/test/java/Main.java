import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Wait;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Main extends Precondition{



    final String URL = "http://user-data.hillel.it/html/registration.html";
    @Test
        public void firstTest() throws InterruptedException {
         //open (URL);
         //clickTabRegistration ();
         //setFirstName("Olha");
         //setLastName ("Babij");
         //setWorkPhone ("7777777");
         //setMobilePhone ("380938712800");
         //setEmail ("obabia@gmail.com");
         //setPassword ("Orange01");
         //clickGender ();
         //clickPosition ();
         //clickButtonRegistration ();
         //clickPopUp ();
         open (URL);
         setPasswordLoginForm ("Orange01");
         setEmailLoginForm ("obabia@gmail.com");
         clickButtonLogin ();
         clickPopupInfo ();


         driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);



    }

 public void open (String url){
        driver.get(URL);
 }
 public void clickTabRegistration (){

     driver.findElement(By.className("registration")).click();
 }
 public void setFirstName (String firstName){
     driver.findElement(By.id("first_name")).sendKeys(firstName);
 }

    public void setLastName (String lastName){
        driver.findElement(By.id("last_name")).sendKeys(lastName);
    }

    public void setWorkPhone (String workPhone){
        driver.findElement(By.id("field_work_phone")).sendKeys(workPhone);
    }

    public void setMobilePhone (String mobilePhone){
        driver.findElement(By.id("field_phone")).sendKeys(mobilePhone);
    }

    public void setEmail (String email){
        driver.findElement(By.id("field_email")).sendKeys(email);
    }

    public void setPassword (String password){
        driver.findElement(By.id("field_password")).sendKeys(password);
    }

    public void clickGender (){
        driver.findElement(By.id("male")).click();
    }

    public void clickPosition (){
        driver.findElement(By.cssSelector("#position>option[value='qa']")).click();
    }

    public void clickButtonRegistration (){
        driver.findElement(By.id("button_account")).click();
    }

    public void clickPopUp (){
        driver.switchTo().alert().accept();
    }

    public void setPasswordLoginForm (String setPassword){
        driver.findElement(By.xpath("//input [@id= 'password']")).sendKeys(setPassword);
    }

    public void setEmailLoginForm (String setEmail){
        driver.findElement(By.xpath("//input [@id= 'email']")).sendKeys(setEmail);
    }

    public void clickButtonLogin (){
        driver.findElement(By.className("login_button")).click();
    }

    public void clickPopupInfo (){
        driver.findElement(By.cssSelector("#info")).click();
    }






    }





