import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Precondition {
    WebDriver driver;
    @BeforeClass
    public static void mainPrecondition() {

        System.setProperty("webdriver.gecko.driver", "C:\\gecko\\geckodriver-v0.26.0-win64\\geckodriver.exe");
    }

    @Before
    public void precondition() {
        driver = new FirefoxDriver();
    }
}
